import { UploadFileConfig } from '../middlewares/upload-files';

/**
 * Controller decorators set routes metadata for controller. Routes metadata is object where each key represents route definition.
 */

/**
 * adds path prefix for controller
 * @param prefix
 */
export const Controller = (prefix = ''): ClassDecorator => {
  return function (target: Function): void {
    Reflect.defineMetadata('prefix', prefix, target);

    if (!Reflect.hasMetadata('routes', target)) {
      Reflect.defineMetadata('routes', {}, target);
    }
  };
};

export interface RouteDefinition {
  path?: string;
  requestMethod?: 'get' | 'post' | 'delete' | 'options' | 'put' | 'patch';
  methodName?: string | symbol;
  authenticate?: boolean;
  params?: string[];
  httpCode?: number;
  uploadFilesConfig?: UploadFileConfig;
}

export interface Routes {
  [k: string]: RouteDefinition;
}

/**
 * sets route method to get and sets path
 */
export const Get = createRequestDecorator('get');
/**
 * sets route method to put and sets path
 */
export const Put = createRequestDecorator('put');
/**
 * sets route method to post and sets path
 */
export const Post = createRequestDecorator('post');
/**
 * sets route method to patch and sets path
 */
export const Patch = createRequestDecorator('patch');
/**
 * sets route method to delete and sets path
 */
export const Delete = createRequestDecorator('delete');

/**
 * creates path method decorator
 * @param requestMethod
 */
function createRequestDecorator(requestMethod: RouteDefinition['requestMethod']) {
  return function (path: string): MethodDecorator {
    return function (target: Record<string, any>, propertyKey: string | symbol): void {
      propertyKey = typeof propertyKey === 'symbol' ? propertyKey.toString() : propertyKey;

      if (!Reflect.hasMetadata('routes', target.constructor)) {
        Reflect.defineMetadata('routes', {}, target.constructor);
      }

      const routes = Reflect.getMetadata('routes', target.constructor) as Routes;

      routes[propertyKey] = {
        ...routes[propertyKey],
        requestMethod: requestMethod,
        path,
        methodName: propertyKey,
      };

      Reflect.defineMetadata('routes', routes, target.constructor);
    };
  };
}

/**
 * sets athorization to true for all controller methods
 */
export function AuthenticateAll(): ClassDecorator {
  return function (target: Function): void {
    if (!Reflect.hasMetadata('routes', target)) {
      return;
    }

    const routes = Reflect.getMetadata('routes', target) as Routes;

    Object.keys(routes).forEach(routeKey => (routes[routeKey].authenticate = true));

    Reflect.defineMetadata('routes', routes, target);
  };
}

/**
 * sets method authentication to true
 */
export function Authenticate(): MethodDecorator {
  return function (target: Record<string, any>, propertyKey: string | symbol): void {
    propertyKey = typeof propertyKey === 'symbol' ? propertyKey.toString() : propertyKey;

    if (!Reflect.hasMetadata('routes', target.constructor)) {
      Reflect.defineMetadata('routes', {}, target.constructor);
    }

    const routes = Reflect.getMetadata('routes', target.constructor) as Routes;
    routes[propertyKey].authenticate = true;

    Reflect.defineMetadata('routes', routes, target.constructor);
  };
}

/**
 * sets method authentication to true
 */
export function UploadFiles(config: UploadFileConfig): MethodDecorator {
  return function (target: Record<string, any>, propertyKey: string | symbol): void {
    propertyKey = typeof propertyKey === 'symbol' ? propertyKey.toString() : propertyKey;

    if (!Reflect.hasMetadata('routes', target.constructor)) {
      Reflect.defineMetadata('routes', {}, target.constructor);
    }

    const routes = Reflect.getMetadata('routes', target.constructor) as Routes;
    routes[propertyKey].uploadFilesConfig = config;

    Reflect.defineMetadata('routes', routes, target.constructor);
  };
}

// controller method params decorators for params injection
export const Body = createParamDecorator('body');
export const Files = createParamDecorator('files');
export const Fields = createParamDecorator('fields');
export const Req = createParamDecorator('req');
export const Res = createParamDecorator('res');
export const Next = createParamDecorator('next');
export const AuthUser = createParamDecorator('authUser');
export const Query = createParamDecorator('query');
export const Headers = createParamDecorator('headers');

/**
 * creates param decorator for param injection
 * @param paramName
 */
function createParamDecorator(paramName: string) {
  return function Body(): ParameterDecorator {
    return function (target: Record<string, any>, propertyKey: string | symbol, parameterIndex: number): void {
      propertyKey = typeof propertyKey === 'symbol' ? propertyKey.toString() : propertyKey;

      if (!Reflect.hasMetadata('routes', target.constructor)) {
        Reflect.defineMetadata('routes', {}, target.constructor);
      }

      const routes = Reflect.getMetadata('routes', target.constructor) as Routes;

      if (routes[propertyKey] === undefined) {
        routes[propertyKey] = {};
      }

      if (routes[propertyKey].params === undefined) {
        routes[propertyKey].params = [];
      }

      routes[propertyKey].params![parameterIndex] = paramName;

      Reflect.defineMetadata('routes', routes, target.constructor);
    };
  };
}

/**
 * sets path status code
 * @param code
 */
export function HttpCode(code: number): MethodDecorator {
  return function (target: Record<string, any>, propertyKey: string | symbol): void {
    propertyKey = typeof propertyKey === 'symbol' ? propertyKey.toString() : propertyKey;

    if (!Reflect.hasMetadata('routes', target.constructor)) {
      Reflect.defineMetadata('routes', {}, target.constructor);
    }

    const routes = Reflect.getMetadata('routes', target.constructor) as Routes;
    if (routes[propertyKey] === undefined) {
      routes[propertyKey] = {};
    }

    routes[propertyKey].httpCode = code;

    Reflect.defineMetadata('routes', routes, target.constructor);
  };
}
